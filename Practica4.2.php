$alumnes=[
        [
        'nom'=>'Adria',
        'pseudonim'=>'aula1',
        'email'=>'adria@exemple',
        'data'=>'28-06-1991',
        'modul'=>'DWES'],
        [ 
        'nom'=>'Josep',
        'pseudonim'=>'alu02',
        'email'=>'josep@exemple.com',
        'data'=>'30-11-1994',
        'modul'=>'DWEC'],
        [
        'nom'=>'Anna',
        'pseudonim'=>'alu03',
        'email'=>'anna@exemple.com',
        'data'=>'18-01-1992',
        'modul'=>'DWEC'
        ]
    ]
    ?>
        <?php
    if($_SERVER['REQUEST_METHOD'] === 'POST'){
        extract($_POST);
        netejar_camp($nom);
        netejar_camp($pseudonim);
        netejar_camp($email);
        netejar_camp($data);
        if(!empty($nom) && !empty($data) && !empty($email)) {
            if(empty($moduls) === true){
                $moduls = array();
            }
            $alumne=['nom'=>$nom,
            'pseudonim'=>$pseudonim,
            'email'=>$email,
            'data'=>$data,
            'modul'=>$modul];
            $alumnes[]=$alumne;
            mostrar_formulari();
        }
        else {
            echo "<p><strong>No has emplenat algun camp obligatori</strong></p>";
            if(empty($moduls) === true)
                $moduls = array();
            mostrar_formulari($nom, $nom, $pseudonim, $email, $data, $moduls);
        }
    } else
        mostrar_formulari();
    function mostrar_formulari($nom="", $pseudonim="", $email="", $data="") {
    ?>
    <form name="input" action="<?= $_SERVER['PHP_SELF'] ?>" method="post">
    <label for="nom">Nom *: </label>
    <input type="text" name="nom" value="<?= $nom ?>" /><br/>
    <label for="nom">Pseudònim: </label>
    <input type="text" name="pseudonim" value="<?= $pseudonim ?>" /><br/>
    <label for="email">e-mail: </label>
    <input type="email" name="email" value="<?= $email ?>" /><br/>
    <label for="data">Data de naixement: </label>
    <input type="date" name="data" value="<?= $data ?>" /><br/>
    <p>Mòduls que cursa:</p>
    <input type="checkbox" name="moduls[]" value="DWES" 
        <?php
            if (in_array("DWES", $moduls))
                print('checked="checked"');
        ?>
    />
    Desenrollament web en entorn servidor<br/>
    <input type="checkbox" name="moduls[]" value="DWEC" 
        <?php
            if (in_array("DWEC", $moduls))
            print('checked="checked"');
        ?>
    />
    Desenrollament web en entorn client <br/><br/>
    <button type="submit" name="enviar">Envia</button>
    </form>
    <?php
    }
    function netejar_camp(&$camp) {
        $camp = trim(htmlspecialchars($camp));
    }
        function mostra_taula($alumnes){
            ?>
            <p><table border="1">
            <tr><th>Nom</th><th>Pseudónim</th><th>e-mail</th><th>Data de naixement</th><th>Modul</th></tr>
            <?php foreach($alumnes as $alumne): ?>
                <tr>
                    <td><?= $alumne['nom']?></td>
                    <td><?= $alumne['pseudonim']?></td>
                    <td><?= $alumne['email']?></td>
                    <td><?= $alumne['data']?></td>
                    <td><?= $alumne['modul']?></td>
                </tr>
             <?php endforeach; ?>
        </table>  </p>
        <?php
        }
        function afegir_dades($nom,$pseudonim,$email,$data,$modul,&$alumnes){ 
            $alumnes[]=['nom'=> $nom,'pseudonim'=> $pseudonim,
                        'email'=> $email,'data' =>$data];
        }
        mostra_taula($alumnes);